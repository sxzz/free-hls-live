import type { Uploader, StorageProvider } from './types'

export const defineUploader = (uploader: Uploader) => uploader
export const defineStorageProvider = (provider: StorageProvider) => provider
